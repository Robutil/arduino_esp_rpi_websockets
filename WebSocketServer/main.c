#include <stdio.h>
#include <stdlib.h>
#include <zconf.h>
#include <pthread.h>
#include <signal.h>

#include "utilities/network/r_websocket.h"
#include "utilities/general/r_debug.h"
#include "utilities/data_structures/r_buffer.h"
#include "utilities/data_structures/format.h"

#define PAYLOAD_LENGTH_OFFSET 1
#define PACKET_SPEED 1

//Containers the server sockets
static struct websock_server server_websocket;
static struct r_sock server_arduino;

//Ease of use
int ard_sock = -1;
int web_sock = -1;

pthread_t t_arduino; //Separate thread for arduino (it's actually the esp chip, but hey)
pthread_mutex_t arduino_socket_lock;
pthread_mutex_t websocket_socket_lock;

#define MANUAL
#ifdef MANUAL

struct r_buffer *ard_buffer = NULL;

void sig_handler(int sig) {
    /* Manually abort server */

    if (sig == SIGINT) {
        r_log("Closing sockets... ");
        if (ard_sock != SOCKET_ERROR) {
            close(ard_sock);
            ard_sock = SOCKET_ERROR;
        }

        if (web_sock != SOCKET_ERROR) {
            close_websocket_client(web_sock);
        }

        close(server_websocket.sock_container.socket);
        close(server_arduino.socket);

        stop_server(server_arduino);
        close_websocket(server_websocket);

        if (ard_buffer != NULL) {
            r_buffer_destroy(ard_buffer);
            ard_buffer = NULL;
        }

        pthread_cancel(t_arduino);

        exit(0);
    } else {
        r_error("SIG");
    }
}

#endif

/**
 * This function sends a packet to the arduino using the specified protocol
 * */
int arduino_send_packet(uint8_t packet_id, void *payload, uint8_t payload_length) {
    if (ard_sock == SOCKET_ERROR) return SOCKET_ERROR;

    payload_length += 1; //Extra byte for payload ID
    if (DEBUG) printf("[ArduinoSendPacket] Sending new packet [%d] > ", packet_id);

    pthread_mutex_lock(&arduino_socket_lock);

    int success = socket_send(ard_sock, &payload_length, 1);
    if (success == SOCKET_ERROR) return SOCKET_ERROR;

    success = socket_send(ard_sock, &packet_id, 1);
    if (success == SOCKET_ERROR) return SOCKET_ERROR;

    success = socket_send(ard_sock, payload, payload_length);
    if (success == SOCKET_ERROR) return SOCKET_ERROR;

    pthread_mutex_unlock(&arduino_socket_lock);
    if (DEBUG) printf("[ArduinoSendPacket] complete!\n");
    return success;
}

/**
 * This function sends a packet to the websocket client using the specified protocol
 * */
int websocket_send_packet(uint8_t packet_id, void *payload, uint8_t payload_length) {
    char *msg = malloc(payload_length + 2);
    msg[0] = packet_id;
    memcpy(&msg[1], payload, payload_length);

    pthread_mutex_lock(&websocket_socket_lock);
    int success = r_send_websocket_message(web_sock, msg, payload_length + 1);
    pthread_mutex_unlock(&websocket_socket_lock);

    free(msg);
    return success;
}

/**
 * This is a thread function which the arduino (esp) connection
 * */
void arduino_handler(int client_socket, struct sockaddr_in address) {
    printf("[ArduinoHandler] Arduino connected! (%d)\n", client_socket);

    ard_sock = client_socket;
    ard_buffer = r_buffer_new();

    while (1) {
        //Read packet
        r_buffer_reset(ard_buffer);
        int error = socket_receive_bytes(client_socket, ard_buffer, 1);
        if (error == SOCKET_ERROR) break;

        r_buffer_mode_read(ard_buffer);
        uint8_t payload_length = (uint8_t) r_buffer_read_int8(ard_buffer);
        if (DEBUG) printf("[ArduinoHandler] Payload size: %d\n", payload_length);
        r_buffer_reset(ard_buffer);

        error = socket_receive_bytes(client_socket, ard_buffer, payload_length);
        if (error == SOCKET_ERROR) {
            printf("[ArduinoHandler] Error while receiving data!\n");
            break;
        }

        r_buffer_mode_read(ard_buffer);
        char *payload = r_buffer_read_string(ard_buffer, payload_length);

        if (payload[0] == 2) {
            short *data = (short *) &payload[1];
            if (DEBUG) printf("[ArduinoHandler] Received LDR packet [%d]\n", *data);

            if (web_sock != SOCKET_ERROR) {
                websocket_send_packet(2, data, 2);
            }
        }
    }
    r_buffer_destroy(ard_buffer);
    ard_buffer = NULL;
    ard_sock = -1;
}


/**
 * This function handles a speed packet from the web by passing it to the arduino.
 * */
void handle_speed_packet_from_web(struct websock_client *client, char *payload, size_t payload_length) {
    int8_t x = (uint8_t) payload[0];
    int8_t y = (uint8_t) payload[1];

    if (DEBUG) printf("[ArduinoHandler](SpeedPacketFromWeb) x: %d y: %d\n", x, y);

    int success = arduino_send_packet(PACKET_SPEED, payload, 2);
    if (success == SOCKET_ERROR) {
        printf("[ArduinoHandler](SpeedPacketFromWeb) Could not send arduino packet. Connection closed?\n");
    }
}

/**
 * This is a thread function which handles an incoming websocket client
 * */
void websocket_handler(struct websock_client *client) {
    web_sock = client->socket;

    while (1) {
        //Read packet
        int status = read_websocket_frame(client);
        if (status == SOCKET_ERROR) break;
        //if (DEBUG) debug_websocket_message(client.p_root_message); //This prints a lot

        //Handle packet
        if (client->p_root_message->fin == WEBSOCK_FINAL_MSG) {
            uint8_t *id = client->p_root_message->data->buffer;
            char *payload = client->p_root_message->data->buffer + PAYLOAD_LENGTH_OFFSET;

            if (DEBUG) printf("[WebsocketHandler] Websock msg, id[%d] msg[%s]\n", *id, payload);

            //If the arduino is connected, do stuff
            if (ard_sock != SOCKET_ERROR) {
                if (*id == PACKET_SPEED) {
                    handle_speed_packet_from_web(client, payload,
                                                 r_buffer_get_length(client->p_root_message->data) -
                                                 PAYLOAD_LENGTH_OFFSET);
                }
            }
        }

        //Clean up
        int type = client->p_root_message->type;
        r_buffer_destroy(client->p_root_message->data);
        free(client->p_root_message);
        client->p_root_message = NULL;

        if (type == 0x8) break; //Termination frame
    }
    force_close_websocket_client(client->socket);
    free(client);
    printf("[WebsocketHandler] Client disconnected\n");
}

void *setup_arduino_server_socket(void *msg) {
    if (DEBUG) printf("[ArduinoHandler] Thread: %s started\n", (char *) msg);

    server_arduino = new_socket("58081", 1);
    start_server(server_arduino, arduino_handler);

    return NULL; //pthread
}

int main() {
    if (pthread_mutex_init(&arduino_socket_lock, NULL) != 0 || pthread_mutex_init(&websocket_socket_lock, NULL) != 0) {
        printf("[Main] mutex init failed\n");
        return 1;
    }


#ifdef MANUAL
    //Manual abort
    if (signal(SIGINT, sig_handler) == SIG_ERR) r_error("SIG");
#endif

    //Setup servers
    pthread_create(&t_arduino, NULL, setup_arduino_server_socket, "T_ARDUINO");

    server_websocket = new_websocket("53232", 1);
    start_websocket(server_websocket, websocket_handler);

    return 0;
}