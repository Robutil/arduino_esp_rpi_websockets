/*
 * r_buffer_safe.h, 2/23/2017.
 *
 * Robbin Siepman <Robbin@Siepman.com>
 * Copyright (c) 2016 <Robutil>
 *
 * Version: -
 * Summary: -
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHOR(S) OR COPYRIGHT 
 * HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 *
 */

//TODO: when writing data in the middle of the buffer, the available length is less than bufsize - datalength

#include <stdlib.h>

#include "r_buffer.h"

#ifndef NO_FLAGS
#define NO_FLAGS 0
#endif

/**
 * Create a buffer with other than standard memory size.
 * Use with care, preferably use r_buffer_new().
 * */
struct r_buffer *r_buffer_new_limited(size_t buffer_size) {

    struct r_buffer *reference = malloc(sizeof(struct r_buffer));
    reference->buffer = malloc(buffer_size);
    memset(reference->buffer, 0, buffer_size);

    reference->length = 0;
    reference->pivot = 0;
    reference->mem_size = buffer_size;

    return reference;
}

/**
 * Creates a new r_buffer with preset buffer size
 * */
struct r_buffer *r_buffer_new() {
    return r_buffer_new_limited(NETWORK_BUFFER_SIZE);
}

/**
 * Frees memory, do not use req after calling this
 * */
void r_buffer_destroy(struct r_buffer *req) {
    if (req == NULL) return;

    free(req->buffer);
    free(req);
}

/**
 * @Depricated
 * Resets buffer contents to default. This only works when
 * the buffer has NETWORK_BUFFER_SIZE size.
 * */
void r_buffer_clear(struct r_buffer *req) {
    //TODO: fix lengths

    memset(req->buffer, NO_FLAGS, NETWORK_BUFFER_SIZE);
    bzero(req->buffer, NETWORK_BUFFER_SIZE); //Obsolete
    req->length = 0;
    req->pivot = 0;
}

/**
 * Removes all data before pivot. Brings rest to start of buffer
 * */
void r_buffer_clean(struct r_buffer *req) {
    if (req->pivot == 0) return; //Nothing to do

    memmove(req->buffer, req->buffer + req->pivot, req->length - req->pivot);
    req->length -= req->pivot;
    req->pivot = 0;
}

/**
 * Adds req buffer to base buffer. The pivot will not be moved if this operation fails
 * */
void r_buffer_append(struct r_buffer *base, struct r_buffer *req) {
    if ((base->pivot + req->length) > base->mem_size) return;

    memcpy(base->buffer + base->pivot, req->buffer + req->pivot, req->length - req->pivot);

    base->length += req->length;
    base->pivot += req->length;
}

/**
 * Resets pivot to 0
 * */
void r_buffer_reset(struct r_buffer *req) {
    req->pivot = 0;
}

/**
 * Brings pivot to front.
 * */
void r_buffer_mode_read(struct r_buffer *req) {
    req->pivot = 0;
}

/**
 * Brings pivot to back.
 * */
void r_buffer_mode_write(struct r_buffer *req) {
    req->pivot = (unsigned int) req->length;
}

/**
 * Puts pivot at the end of content. This will invoke behavior as if the buffer is full, hence
 * locking it for writes.
 * */
void r_buffer_lock(struct r_buffer *req) {
    req->pivot = (unsigned int) req->length;
}

/**
 * Returns number of bytes left unread.
 * */
size_t r_buffer_get_length(struct r_buffer *req) {
    return req->length - req->pivot;
}

/**
 * Reads @param size data from buffer. The pivot will not be moved if this operation fails
 * */
void *r_buffer_read(struct r_buffer *req, size_t size) {
    if (req->pivot + size > req->length) return 0;

    //Allocate memory
    void *res = calloc(size, 1);

    //Transfer size to newly allocated memory
    memcpy(res, req->buffer + req->pivot, size);

    //Move pivot for future use
    req->pivot += size;

    return res;
}

/**
 * Reads 8 bit integer from req. The pivot will not be moved if this operation fails
 * */
int8_t r_buffer_read_int8(struct r_buffer *req) {
    if (req->pivot + sizeof(int8_t) > req->length) return 0;

    int8_t res;
    memcpy(&res, req->buffer + req->pivot, sizeof(int8_t));
    req->pivot += sizeof(int8_t);

    return res;
}

/**
 * Reads 32 bit integer from req. The pivot will not be moved if this operation fails.
 * This operation does not deal with endianess.
 * */
int32_t r_buffer_read_int32(struct r_buffer *req) {
    if (req->pivot + sizeof(int32_t) > req->length) return 0;

    int32_t res;
    memcpy(&res, req->buffer + req->pivot, sizeof(int32_t));
    req->pivot += sizeof(int32_t);

    return res;
}

/**
 * Reads 64 bit integer from req. The pivot will not be moved if this operation fails.
 * This operation does not deal with endianess.
 * */
int64_t r_buffer_read_int64(struct r_buffer *req) {
    if (req->pivot + sizeof(int64_t) > req->length) return 0;

    int64_t res;
    memcpy(&res, req->buffer + req->pivot, sizeof(int64_t));
    req->pivot += sizeof(int64_t);

    return res;
}

/**
 * Reads string of defined length from req. The pivot will not be moved if this operation fails.
 * */
char *r_buffer_read_string(struct r_buffer *req, size_t length) {
    if (req->pivot + length > req->length) return NULL;

    char *res = malloc(length + 1);

    memcpy(res, req->buffer + req->pivot, length);
    res[length] = 0; //Null terminated string, according to posix

    req->pivot += length;
    return res;
}

/**
 * Reads data for length and returns a new struct r_buffer. Zero reads all.
 * */
struct r_buffer *r_buffer_read_data(struct r_buffer *req, size_t length) {
    if (req->pivot + length > req->length) return NULL;

    if (length == 0) length = req->length - req->pivot;
    struct r_buffer *data = r_buffer_new_limited(length);
    data->length = length;

    memcpy(data->buffer, req->buffer + req->pivot, length);
    req->pivot += length;

    return data;
}

/**
 * @Depricated
 * Writes data to req. The pivot will not be moved if this operation fails.
 * */
void r_buffer_write(struct r_buffer *req, void *data, size_t length) {
    if (req->pivot + length > NETWORK_BUFFER_SIZE) return;

    //TODO: check for buffer memory size instead of global constant

    memcpy(req->buffer + req->pivot, data, length);

    req->length += length;
    req->pivot += length;
}

/**
 * @Depricated
 * Writes int_16 to req. The pivot will not be moved if this operation fails
 * */
void r_buffer_write_int16(struct r_buffer *req, int32_t value) {
    if (req->pivot + sizeof(int16_t) > NETWORK_BUFFER_SIZE) return;

    //TODO: check for buffer memory size instead of global constant

    memcpy(req->buffer + req->pivot, &value, sizeof(int16_t));
    req->length += sizeof(int16_t);
    req->pivot += sizeof(int16_t);
}

/**
 * @Depricated
 * Writes int_32 to req. The pivot will not be moved if this operation fails
 * */
void r_buffer_write_int(struct r_buffer *req, int32_t value) {
    if (req->pivot + sizeof(int32_t) > NETWORK_BUFFER_SIZE) return;

    //TODO: check for buffer memory size instead of global constant

    memcpy(req->buffer + req->pivot, &value, sizeof(int32_t));
    req->length += sizeof(int32_t);
    req->pivot += sizeof(int32_t);
}

/**
 * @Depricated
 * Writes int_64 to req. The pivot will not be moved if this operation fails.
 * */
void r_buffer_write_int64(struct r_buffer *req, int64_t value) {
    if (req->pivot + sizeof(int64_t) > NETWORK_BUFFER_SIZE) return;

    //TODO: check for buffer memory size instead of global constant

    memcpy(req->buffer + req->pivot, &value, sizeof(int64_t));
    req->length += sizeof(int64_t);
    req->pivot += sizeof(int64_t);
}

/**
 * Writes string of specified length to req. The pivot will not be moved if this operation fails
 * */
void r_buffer_write_string(struct r_buffer *req, const char *req_string, size_t length) {
    if (req->pivot + length > req->mem_size) return;

    memcpy(req->buffer + req->pivot, req_string, length);

    req->pivot += length;
    req->length += length;
}


/**
 * Returns amount of bytes available in buffer.
 * */
size_t r_buffer_get_available(struct r_buffer *req) {
    return req->mem_size - req->length;
}