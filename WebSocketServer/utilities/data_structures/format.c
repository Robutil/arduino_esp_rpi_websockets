#include <string.h>
#include <stdlib.h>
#include <ctype.h>

/*
 * format.h, 6/11/2016.
 *
 * Robbin Siepman <Robbin@Siepman.com>
 * Copyright (c) 2016 <Robutil>
 *
 * Version: 1.0
 * Summary: Format contains utility functions that are required
 * for finding id numbers. It converts ASCII integer values to
 * a int32_t value as per format.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHOR(S) OR COPYRIGHT
 * HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

#include "format.h"

int unwrap_id(char *req, int start, char split) {
    /* Returns integer value of ascii digits before split token. Returns -1 on failure. */

    int i, temp_id = 0, flag = 1;
    int negative = 0;

    for (i = start; i < strlen(req); i++) {
        if (req[i] == '-') {
            negative = 1;
            i++;
        } else if (!isdigit(req[i])) return -1;

        temp_id += req[i] - '0';
        if (req[i + 1] == split) {
            flag = 0;
            break;
        }
        temp_id *= 10;
    }
    if (flag) return -1; //No split token found

    if (negative) return 0 - temp_id;
    return temp_id;
}