#ifndef WEBSOCKETSERVER_R_BUFFER_SAFE_H
#define WEBSOCKETSERVER_R_BUFFER_SAFE_H

#include <stdint.h>
#include <string.h>

#define NETWORK_BUFFER_SIZE 4096

struct r_buffer {
    size_t mem_size;
    void *buffer;
    size_t length;
    unsigned int pivot;
};

struct r_buffer *r_buffer_new();

struct r_buffer *r_buffer_new_limited(size_t buffer_size);

void r_buffer_destroy(struct r_buffer *req);

void r_buffer_clear(struct r_buffer *req);

void r_buffer_clean(struct r_buffer *req);

void r_buffer_append(struct r_buffer *base, struct r_buffer *req);

void r_buffer_reset(struct r_buffer *req);

void r_buffer_lock(struct r_buffer *req);

size_t r_buffer_get_length(struct r_buffer *req);

void r_buffer_write(struct r_buffer *req, void *data, size_t length);

int32_t r_buffer_read_int(struct r_buffer *req, int peek);

int8_t r_buffer_read_int8(struct r_buffer *req);

char *r_buffer_read_string(struct r_buffer *req, size_t length);

struct r_buffer *r_buffer_read_data(struct r_buffer *req, size_t length);

void r_buffer_write_int(struct r_buffer *req, int32_t value);

void r_buffer_write_string(struct r_buffer *req, const char *req_string, size_t length);

size_t r_buffer_get_available(struct r_buffer *req);

void r_buffer_mode_read(struct r_buffer *req);

void r_buffer_mode_write(struct r_buffer *req);
#endif //WEBSOCKETSERVER_R_BUFFER_SAFE_H
