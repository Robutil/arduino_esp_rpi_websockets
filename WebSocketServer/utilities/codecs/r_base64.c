#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#include "r_base64.h"

static char *BASE64_CODECS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

/**
 * Returns the length of a decoded base64 string
 */
static size_t r_b64_get_length(const char *b64_input) {
    size_t len = strlen(b64_input);

    size_t padding = 0;
    if (b64_input[len - 1] == 0x3D) {
        if (b64_input[len - 2] == 0x3D) padding = 2;
        else padding = 1;
    }

    return (len * 3) / 4 - padding;
}

/**
 * Searches the base64 codes for decoded character and returns corresponding base64 codecs value.
 * In case a value is not found it returns the base64 '=' value: 64.
 * */
uint8_t r_base64_get_codec_index(char find) {
    for (uint8_t i = 0; i < strlen(BASE64_CODECS); ++i) {
        if (BASE64_CODECS[i] == find) return i;
    }
    return 64;
}

char *r_base64_decode(char *b64_encoded, size_t *length) {
    if (length == NULL) length = malloc(sizeof(size_t));
    *length = r_b64_get_length(b64_encoded);

    char *b64_decoded = malloc(*length + 1);

    uint8_t *helper = malloc(4);
    int counter = 0;

    for (int i = 0; i < strlen(b64_encoded); i += 4) {
        for (int j = 0; j < 4; ++j) {
            helper[j] = (uint8_t) r_base64_get_codec_index(b64_encoded[i + j]);
        }

        //First octet
        b64_decoded[counter++] = ((helper[0] << 2) | (helper[1] >> 4));

        //Second octet
        if (helper[2] < 64) b64_decoded[counter++] = ((helper[1] << 4) | (helper[2] >> 2));

        //Third octet
        if (helper[3] < 64)b64_decoded[counter++] = ((helper[2] << 6) | (helper[3]));
    }

    free(helper);
    return b64_decoded;
}

char *r_base64_encode(unsigned char *b64_decoded, size_t length) {
    size_t length_encoded = (length * 4) / 3;
    size_t length_decoded = length;

    char *b64_encoded = malloc(length_encoded + 1);
    b64_encoded[length_encoded] = 0;

    uint8_t helper;
    size_t counter = 0;
    for (int i = 0; i < length_decoded; i += 3) {

        //First octet is always there
        helper = (uint8_t) ((b64_decoded[i] & 0xFC) >> 2);
        b64_encoded[counter++] = BASE64_CODECS[helper];

        //Prep second octet
        helper = (uint8_t) ((b64_decoded[i] & 0x03) << 4);

        if (i + 1 < length_decoded) {
            //Finish second octet
            helper |= (b64_decoded[i + 1] & 0xF0) >> 4;
            b64_encoded[counter++] = BASE64_CODECS[helper];

            //Prep third octet
            helper = (uint8_t) ((b64_decoded[i + 1] & 0x0F) << 2);

            if (i + 2 < length_decoded) {
                //Finish third octet
                helper |= (b64_decoded[i + 2] & 0xC0) >> 6;
                b64_encoded[counter++] = BASE64_CODECS[helper];

                //Do fourth octet
                helper = (uint8_t) (b64_decoded[i + 2] & 0x3F);
                b64_encoded[counter++] = BASE64_CODECS[helper];

            } else {
                //No more bytes to process, finish prematurely
                b64_encoded[counter++] = BASE64_CODECS[helper];
                b64_encoded[counter++] = '=';
            }
        } else {
            //No more bytes to process, finish prematurely
            b64_encoded[counter++] = BASE64_CODECS[helper];
            b64_encoded[counter++] = '=';
            b64_encoded[counter++] = '=';
        }
    }
    b64_encoded[counter] = 0;
    printf("b64: %s\n", b64_encoded);
    return b64_encoded;
}
