#ifndef R_BASE64_H
#define R_BASE64_H

/* Encodes and Decodes base64. These functions cannot handle corrupted input. */
char *r_base64_decode(char *b64_encoded, size_t *length);
char *r_base64_encode(unsigned char *b64_decoded, size_t length);

#endif
