#ifndef WEBSOCKETSERVER_APPLE_BASE_H
#define WEBSOCKETSERVER_APPLE_BASE_H


int Base64encode_len(int len);

int Base64encode(char *encoded, const char *string, int len);

#endif //WEBSOCKETSERVER_APPLE_BASE_H
