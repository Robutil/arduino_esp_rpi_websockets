#include "r_socket.h"

#include <zconf.h>

#include "../general/r_debug.h"

#ifdef USE_R_BUFFER

int socket_receive(int socket, struct r_buffer *buffer) {
    int temp = (int) recv(socket, buffer->buffer + buffer->pivot, r_buffer_get_available(buffer), 0);
    if (temp >= 0) {
        buffer->pivot += temp;
        buffer->length += temp;
    }

    return temp;
}

int socket_receive_bytes(int socket, struct r_buffer *buffer, size_t bytes) {
    int64_t status = 0;
    while (buffer->length != buffer->mem_size) {
        status = recv(socket, buffer->buffer + buffer->pivot, bytes, 0);
        if (status == SOCKET_ERROR) return SOCKET_ERROR;

        buffer->pivot += status;
        buffer->length += status;

        bytes -= status;
        if (bytes == 0) break;
    }

    return 0;
}

#endif

int socket_send(int socket, void *data, size_t length) {
    int bytes_to_send = (int) length;

    while (bytes_to_send != 0) {
        int temp = (int) send(socket, data + (length - bytes_to_send), (size_t) bytes_to_send, 0);
        if (temp == SOCKET_ERROR) return SOCKET_ERROR;
        else bytes_to_send -= temp;
    }
}

struct r_sock new_socket(char *port, int max_client) {
    struct r_sock new_socket;

    struct addrinfo data, *server_info;
    new_socket.max_clients = max_client;

    //Set sock_container flags
    memset(&data, 0, sizeof(data));
    data.ai_family = AF_INET;
    data.ai_socktype = SOCK_STREAM;
    data.ai_flags = AI_PASSIVE;

    /* Basic TCP server initialisation */

    if (getaddrinfo(NULL, port, &data, &server_info) != 0) {
        r_error_soft("AddressInfo");
        new_socket.socket = -1;
        return new_socket;
    }

    new_socket.socket = socket(server_info->ai_family, server_info->ai_socktype, server_info->ai_protocol);

    if (new_socket.socket < 0) {
        r_error_soft("Socket");
        return new_socket;
    }

    const int reuse = 1;
    if (setsockopt(new_socket.socket, SOL_SOCKET, SO_REUSEADDR, (const char *) &reuse, sizeof(reuse)) < 0)
        r_error_soft("setsockopt(SO_REUSEADDR) failed");

#ifdef SO_REUSEPORT
    if (setsockopt(new_socket.socket, SOL_SOCKET, SO_REUSEPORT, (const char*)&reuse, sizeof(reuse)) < 0)
        r_error_soft("setsockopt(SO_REUSEPORT) failed");
#endif

    if (bind(new_socket.socket, server_info->ai_addr, server_info->ai_addrlen) == -1) {
        r_error_soft("Bind");
        new_socket.socket = -1;
        return new_socket;
    }

    freeaddrinfo(server_info);
    return new_socket;
}

void start_server(struct r_sock server, void (*f_on_client)(int, struct sockaddr_in)) {
    if (server.socket == SOCKET_ERROR) return;

    //Start listening on the server sock_container
    if (listen(server.socket, server.max_clients) < 0) r_error("listen");

    server.running = 1;

    struct sockaddr_in client_address;
    size_t malloc_size = sizeof(struct sockaddr_in);

    while (server.running) {
        int new_client = accept(server.socket, (struct sockaddr *) &client_address, (socklen_t *) &malloc_size);
        if (new_client != SOCKET_ERROR) {
            f_on_client(new_client, client_address);
        } else {
            //r_socket_error("Accept");
        }
    }
    stop_server(server);
}

int stop_server(struct r_sock server) {
    server.running = 0;
    int temp = close(server.socket);
    server.socket = SOCKET_ERROR;
    return temp;
}

