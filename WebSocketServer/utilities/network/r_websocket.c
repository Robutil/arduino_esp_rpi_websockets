/*
 * r_websock.h, 2017-06-13.
 *
 * Robbin Siepman <Robbin@Siepman.com>
 * Copyright (c) 2016 <Robutil>
 *
 * Version: -
 * Summary: -
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHOR(S) OR COPYRIGHT
 * HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

#include "r_websocket.h"

#include <elf.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>

#include <openssl/sha.h>
#include <zconf.h>

#include "../general/r_debug.h"
#include "../codecs/r_base64.h"
#include "../general/r_constants.h"

#include "../general/r_debug.h"
#include "../codecs/apple_base.h"
#include "../general/r_constants.h"

#define HTTP_REQ_MAX_LENGTH 2000
#define MASKING_KEY_SIZE 4

//Websocket constants
const char *GUID = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
const size_t GUID_LENGTH = 0x24;


void debug_websocket_message(struct websock_message *msg) {
    if (msg == NULL) {
        printf("Received (null)\n");

    } else if (msg->data->length < 10) {
        printf("Received %d bytes\t type: %x\t fin: %x\t payload: %s\n", (int) msg->data->length,
               msg->type, msg->fin, (char *) msg->data->buffer);
    } else {
        printf("Received %d bytes\t type: %x\t fin: %x\t payload: [shortened]\n", (int) msg->data->length,
               msg->type, msg->fin);
    }
}

int r_send_websocket_message(int socket, void *msg, size_t length) {
    if (length > 125) {
        printf("[SendWebsocketMessage] Message too long to send\n");
        return SOCKET_ERROR;
    }
    uint8_t websocket_message[2];

    websocket_message[0] = 0;
    websocket_message[0] |= 1 << 7; //Final message
    websocket_message[0] |= 1 << 1; //Binary message

    websocket_message[1] = (uint8_t) length;

    int success = socket_send(socket, websocket_message, 2);
    if (success == SOCKET_ERROR)
        return SOCKET_ERROR;

    success = socket_send(socket, msg, length);
    if (success == SOCKET_ERROR)
        return SOCKET_ERROR;
}

int send_websocket_message(struct websock_client *client, void *msg, size_t length) {
    return r_send_websocket_message(client->socket, msg, length);
}

static void handshake_response(char *response, char *key_response) {
    sprintf(response, "HTTP/1.1 101 Switching Protocols\r\n"
            "Upgrade: websocket\r\n"
            "Connection: Upgrade\r\n"
            "Sec-WebSocket-Accept: %s\r\n\r\n", key_response);
}

static char *extract_value_from_http_req(char *http_req, char *key) {
    int key_index = 0;
    for (int i = 0; i < strlen(http_req); ++i) {
        if (http_req[i] == key[key_index]) {
            key_index++;
            if (key_index == strlen(key)) {
                while (http_req[i] != 0x20 && i < strlen(http_req)) i++; //Find start of value
                for (key_index = ++i; key_index < strlen(http_req); ++key_index) {
                    if (http_req[key_index] == 0x20 ^ http_req[key_index] == 0xD) {
                        char *value = malloc((size_t) key_index - i + 1);
                        if (value == NULL) {
                            printf("[ExtractHttpValue] Unable to malloc\n");
                            return NULL;
                        }

                        for (int j = 0; j < key_index - i; ++j) {
                            value[j] = http_req[i + j];
                        }
                        //memcpy(value, http_req + i, (size_t) key_index - i);
                        value[key_index - i] = 0; //Null termination
                        return value;
                    }
                }
            }
        } else {
            key_index = 0;
        }
    }
    return NULL;
}

static void send_http_response(struct websock_client *client) {
    if (client->network_buffer->length == 0) return;

    //Unspecified behaviour
    r_buffer_mode_read(client->network_buffer);

    //First get clients generated key
    void *http_key_request = extract_value_from_http_req(client->network_buffer->buffer, "Sec-WebSocket-Key");
    if (!http_key_request) return;

    //Concat client key with websocket constant
    char *key_concat = calloc(GUID_LENGTH + strlen(http_key_request) + 2, 1);
    strcat(key_concat, http_key_request);
    strcat(key_concat, GUID);

    free(http_key_request);

    //Calculate hash of concat result
    unsigned char hash[SHA_DIGEST_LENGTH];
    SHA1((const unsigned char *) key_concat, strlen(key_concat), hash);
    free(key_concat);

    char key_response[Base64encode_len(SHA_DIGEST_LENGTH)];
    int size = Base64encode(key_response, (const char *) hash, SHA_DIGEST_LENGTH);

    char temp[256];
    handshake_response(temp, key_response);

    socket_send(client->socket, temp, strlen(temp));
}

static void read_incoming_http_request(struct websock_client *client) {
    int status, flag = 1;

    //Read incoming http req
    while (flag) {
        status = socket_receive(client->socket, client->network_buffer);

        if (status < 0) {
            r_buffer_clear(client->network_buffer);
            flag = 0;
        }

        if (status > 4) {
            char *last_four_bytes = client->network_buffer->buffer + client->network_buffer->pivot - 4;

            //If a double \r\n occurs, there is a full http request
            if (last_four_bytes[0] == 0xD && last_four_bytes[1] == 0xA) {
                if (last_four_bytes[0] == last_four_bytes[2] && last_four_bytes[1] == last_four_bytes[3]) {
                    flag = 0;
                }
            }
        }
    }
}

static void handle_websocket_handshake(struct websock_client *client, void (*f_on_client)(struct websock_client *)) {
    client->network_buffer = r_buffer_new_limited(HTTP_REQ_MAX_LENGTH);

    read_incoming_http_request(client);
    send_http_response(client);

    r_buffer_destroy(client->network_buffer);
    client->network_buffer = NULL;

    f_on_client(client); //TODO: make this a pointer
}

static void handle_accept_error(struct websock_server server) {
    //Stub
}

struct websock_message *new_websock_message() {
    struct websock_message *new = malloc(sizeof(struct websock_message));
    new->p_message = NULL;
    new->data = NULL;
    new->type = 0;
    return new;
}

struct websock_message *new_message_spot(struct websock_client *client) {
    if (client->p_root_message == NULL) {
        client->p_root_message = new_websock_message();
        return client->p_root_message;
    }

    struct websock_message *message = client->p_root_message;

    //Find last filled node
    while (message->p_message != NULL) message = message->p_message;

    message->p_message = new_websock_message();
    message = message->p_message;

    return message;
}

int read_websocket_frame_payload(struct websock_client *client) {
    r_buffer_destroy(client->network_buffer);
    client->network_buffer = r_buffer_new_limited(client->frame->PAYLOAD_LENGTH + 1);

    //Receive message
    int status = socket_receive_bytes(client->socket, client->network_buffer, client->frame->PAYLOAD_LENGTH);
    if (status == SOCKET_ERROR) return SOCKET_ERROR;

    //Create message spot in client struct
    struct websock_message *message = new_message_spot(client);
    message->type = client->frame->OPCODE;
    message->fin = client->frame->FIN;

    //Change network buffer to message buffer
    message->data = client->network_buffer;
    client->network_buffer = NULL;

    //Decode message
    uint8_t *data = message->data->buffer;

    for (int i = 0; i < client->frame->PAYLOAD_LENGTH; ++i) {
        data[i] = data[i] ^ client->frame->key[i % 4];
    }
    data[client->frame->PAYLOAD_LENGTH] = 0;

    free(client->frame->key);
    free(client->frame);
}

int read_websocket_frame_payload_length_and_key(struct websock_client *client) {
    r_buffer_destroy(client->network_buffer);

    //Determine next header sizes
    if (client->frame->PAYLOAD_LENGTH_SHORT < 126) {
        client->network_buffer = r_buffer_new_limited(4);
    } else if (client->frame->PAYLOAD_LENGTH_SHORT == 126) {
        client->network_buffer = r_buffer_new_limited(6);
    } else {
        client->network_buffer = r_buffer_new_limited(12);
    }

    //Read next header sizes
    int status = 0;
    while (client->network_buffer->length != client->network_buffer->mem_size && status != SOCKET_ERROR) {
        status = socket_receive(client->socket, client->network_buffer);
    }
    if (status == SOCKET_ERROR) return SOCKET_ERROR;
    r_buffer_mode_read(client->network_buffer);

    //Determine payload length
    if (client->frame->PAYLOAD_LENGTH_SHORT < 126) {
        client->frame->PAYLOAD_LENGTH = client->frame->PAYLOAD_LENGTH_SHORT;

    } else if (client->frame->PAYLOAD_LENGTH_SHORT == 126) {
        uint16_t *p_temp = (client->network_buffer->buffer);
        reverse_endianess(p_temp, sizeof(uint16_t));
        client->frame->PAYLOAD_LENGTH = *p_temp;
        client->network_buffer->pivot += sizeof(uint16_t);

    } else if (client->frame->PAYLOAD_LENGTH_SHORT == 127) {
        uint64_t *p_temp = (client->network_buffer->buffer);
        reverse_endianess(p_temp, sizeof(uint64_t));
        client->frame->PAYLOAD_LENGTH = *p_temp;
        client->network_buffer->pivot += sizeof(uint64_t);
    }

    //Get masking key
    client->frame->key = malloc(MASKING_KEY_SIZE);
    memcpy(client->frame->key, client->network_buffer->buffer + client->network_buffer->pivot, MASKING_KEY_SIZE);

    return 0;
}

struct websocket_frame *read_websocket_frame_headers(struct r_buffer *buffer) {
    if (buffer->length - buffer->pivot < 2) return NULL;

    struct websocket_frame *frame = malloc(sizeof(struct websocket_frame));
    uint8_t *p_byte = buffer->buffer + buffer->pivot;

    frame->FIN = CHECK_BIT((uint8_t) *p_byte, BITLOC_FIN);
    frame->RSV1 = CHECK_BIT((uint8_t) *p_byte, BITLOC_RSV1);
    frame->RSV2 = CHECK_BIT((uint8_t) *p_byte, BITLOC_RSV2);
    frame->RSV3 = CHECK_BIT((uint8_t) *p_byte, BITLOC_RSV3);
    frame->OPCODE = (uint8_t) ((uint8_t) *p_byte & BITMASK_OPCODE);

    p_byte = (p_byte + 1);

    frame->MASK = CHECK_BIT((uint8_t) *(p_byte), BITLOC_MASK);
    frame->PAYLOAD_LENGTH_SHORT = (uint8_t) ((uint8_t) *(p_byte) & 0x7F);

    return frame;
}

/**
 * This is a blocking call.
 * */
int read_websocket_frame(struct websock_client *client) {
    //Create buffer for receiving websocket frame header
    if (client->network_buffer != NULL) {
        r_buffer_destroy(client->network_buffer);
    }
    client->network_buffer = r_buffer_new_limited(WEBSOCK_FRAME_SIZE);

    //Receive websocket frame header
    int status = socket_receive_bytes(client->socket, client->network_buffer, WEBSOCK_FRAME_SIZE);
    if (status == SOCKET_ERROR) return -1;

    //Parse websocket frame headers
    r_buffer_mode_read(client->network_buffer);
    client->frame = read_websocket_frame_headers(client->network_buffer);
    if (client->frame == NULL) return -1;

    //Read message according to frame headers
    status = read_websocket_frame_payload_length_and_key(client);
    if (status == SOCKET_ERROR) return SOCKET_ERROR;
    return read_websocket_frame_payload(client);
}

void start_websocket(struct websock_server server_data, void (*f_on_client)(struct websock_client *)) {
    if (server_data.sock_container.socket == SOCKET_ERROR) return;

    if (listen(server_data.sock_container.socket, server_data.sock_container.max_clients) < 0) r_error("listen");

    server_data.sock_container.running = 1;

    struct sockaddr_in client_address;
    size_t malloc_size = sizeof(struct sockaddr_in);

    while (server_data.sock_container.running) {
        printf("[StartWebsocket] Waiting for accept!\n");
        int new_client = accept(server_data.sock_container.socket, (struct sockaddr *) &client_address,
                                (socklen_t *) &malloc_size);

        printf("[StartWebsocket] New client\n");

        if (new_client != SOCKET_ERROR) {
            struct websock_client *client = malloc(sizeof(struct websock_client)); //TODO: make this a pointer
            client->info = client_address;
            client->socket = new_client;

            client->network_buffer = NULL;
            client->frame = NULL;
            client->p_root_message = NULL;

            handle_websocket_handshake(client, f_on_client);
        } else {
            handle_accept_error(server_data);
        }
    }
}

struct websock_server new_websocket(char *port, int max_clients) {
    struct websock_server ret;
    ret.sock_container = new_socket(port, max_clients);
    ret.flags = 0;
    return ret;
}

int force_close_websocket_client(int socket) {
    int success = shutdown(socket, SHUT_WR);
    uint8_t buffer[1];
    while (recv(socket, buffer, 1, NO_FLAGS) > 0);
    close(socket);
    socket = SOCKET_ERROR;
    return success;
}

int close_websocket_client(int socket) {
    if (socket == SOCKET_ERROR) return SOCKET_ERROR;
    uint8_t websocket_message[2];

    websocket_message[0] = 0;
    websocket_message[0] |= 1 << 7; //Final message
    websocket_message[0] |= 1 << 3; //End frame

    websocket_message[1] = 0;

    int success = socket_send(socket, websocket_message, 2);
    //Do not really care if the socket send the final message successfully

    force_close_websocket_client(socket);
}

int close_websocket(struct websock_server server) {
    return stop_server(server.sock_container);
}

