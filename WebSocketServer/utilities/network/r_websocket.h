#ifndef WEBSOCKETSERVER_R_WEBSOCKET_H
#define WEBSOCKETSERVER_R_WEBSOCKET_H

//Required for socketaddr_in
#include <netdb.h>

#include "../data_structures/r_buffer.h"
#include "r_socket.h"

#define WEBSOCK_FINAL_MSG 1

//Bit locations count from zero to seven
#define BITLOC_FIN 7
#define BITLOC_RSV1 6
#define BITLOC_RSV2 5
#define BITLOC_RSV3 4
#define BITLOC_MASK 7
#define BITMASK_OPCODE 0xF
#define BITMASK_PAYLOAD_SHORT 0x7F
#define WEBSOCK_FRAME_SIZE 2

struct websocket_frame {
    uint8_t FIN;
    uint8_t RSV1;
    uint8_t RSV2;
    uint8_t RSV3;
    uint8_t OPCODE;
    uint8_t MASK;
    uint8_t PAYLOAD_LENGTH_SHORT;
    uint8_t *key;
    uint64_t PAYLOAD_LENGTH;
};


struct websock_message {
    uint8_t type;
    uint8_t fin;
    struct r_buffer *data;
    struct websock_message *p_message;
};

struct websock_client {
    struct sockaddr_in info;
    int socket;

    struct websocket_frame *frame;
    struct r_buffer *network_buffer;

    struct websock_message *p_root_message;
};

struct websock_server {
    struct r_sock sock_container;
    int active;
    uint16_t flags; //possible future extensions
};

void debug_websocket_message(struct websock_message *msg);

struct websock_server new_websocket(char *port, int max_clients);

int send_websocket_message(struct websock_client *client, void *msg, size_t length);

int r_send_websocket_message(int socket, void *msg, size_t length);

void start_websocket(struct websock_server server_data, void (*f_on_client)(struct websock_client *));

int close_websocket(struct websock_server server);

int force_close_websocket_client(int socket);

int close_websocket_client(int socket);

int read_websocket_frame(struct websock_client *client);

#endif //WEBSOCKETSERVER_R_WEBSOCKET_H
