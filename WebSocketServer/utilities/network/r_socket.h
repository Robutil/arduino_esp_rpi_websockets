#ifndef WEBSOCKETSERVER_R_SOCKET_H
#define WEBSOCKETSERVER_R_SOCKET_H

#include <netdb.h>

#define SOCKET_ERROR -1
#define USE_R_BUFFER

struct r_sock {
    int socket;
    int max_clients;
    int running;
};

#ifdef USE_R_BUFFER

#include "../data_structures/r_buffer.h"

int socket_receive(int socket, struct r_buffer *buffer);

int socket_receive_bytes(int socket, struct r_buffer *buffer, size_t bytes);

#endif

int socket_send(int socket, void *data, size_t length);

struct r_sock new_socket(char *port, int max_client);

void start_server(struct r_sock server, void (*f_on_client)(int, struct sockaddr_in));

int stop_server(struct r_sock server);

#endif //WEBSOCKETSERVER_R_SOCKET_H
