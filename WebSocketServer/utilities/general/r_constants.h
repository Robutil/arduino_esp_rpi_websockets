#ifndef WEBSOCKETSERVER_R_CONSTANTS_H
#define WEBSOCKETSERVER_R_CONSTANTS_H

#include <stdint.h>

#define NO_FLAGS 0
#define R_ERROR -1 //Its better to define separate error codes, even if values overlap
#define TRUE 1
#define FALSE 0

#define CHECK_BIT(var, pos) (uint8_t) (((var) & ( 1 << (pos))) == (1 << (pos)))

#endif //WEBSOCKETSERVER_R_CONSTANTS_H
