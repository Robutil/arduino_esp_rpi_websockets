/*
 * sleep.h, 6/11/2016.
 *
 * Robbin Siepman <Robbin@Siepman.com>
 * Copyright (c) 2016 <Robutil>
 *
 * Version: 1.0
 * Summary: Usleep is deprecated since it does not recover after
 * an interrupt. Nanosleep does, and is implemented here.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHOR(S) OR COPYRIGHT
 * HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */


#if __STDC_VERSION__ >= 199901L
#define _XOPEN_SOURCE 600
#else
#define _XOPEN_SOURCE 500
#endif /* __STDC_VERSION__ */

#include <time.h>
#include <stdio.h>
#include <errno.h>

#include "r_sleep.h"

void sleep_mil(long msec) {
    sleep_mic(msec * 1000);
}

void sleep_mic(long long msec) {
    long long usec = msec * 1000L;
    struct timespec time, inter;
    time.tv_sec = usec / 1000000000L;
    time.tv_nsec = usec % 1000000000L;
    int ret_val = nanosleep(&time, &inter);
    while (ret_val == -1 && errno == EINTR) {
        time = inter;
        inter = (struct timespec) {0};
        ret_val = nanosleep(&time, &inter);
    }
}

unsigned long long get_time() {
    struct timespec time;
    clock_gettime(CLOCK_MONOTONIC, &time);
    return time.tv_sec * 1000000L + time.tv_nsec / 1000L;
}

unsigned long long log_time_diff(unsigned long long t, int p) {
    unsigned long long diff = get_time() - t;
    if (p) printf("Time difference %llu\n", diff);
    return diff;
}