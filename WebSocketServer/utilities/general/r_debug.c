/*
 * robutil.h, 6/11/2016.
 *
 * Robbin Siepman <Robbin@Siepman.com>
 * Copyright (c) 2016 <Robutil>
 *
 * Version: 1.0
 * Summary: Mostly debugging functions.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHOR(S) OR COPYRIGHT
 * HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "r_debug.h"

void r_error_soft(char *mess) {
    perror(mess);
}

void r_error(char *mess) {
    r_error_soft(mess);
    exit(1);
}

void r_error_ld_soft(int pivot, char *mess) {
    if (pivot) r_error_soft(mess);
}

void r_error_ld(int pivot, char *mess) {
    if (pivot) r_error(mess);
}

void r_log(char *mess) {
    if (mess == NULL) r_log("r_log(NULL);");
    else printf("%s\n", mess);
}

void r_debug(int req, char *mess) {
    if (req) r_log(mess);
}

void r_debug_val(int req, char *mess, int val) {
    if (req) r_log_val(mess, val);
}

void r_log_val(char *mess, int val) {
    printf("%s: %d\n", mess, val);
}

void r_clear(char *mess) {
    bzero(mess, strlen(mess));
    memset(mess, 0, sizeof(*mess));
}

const char *byte_to_binary(uint8_t x) {
    static char b[9];
    b[8] = '\0';

    for (int i = 0; i < 8; ++i) {
        b[7 - i] = (char) ((x & 1 << i) ? '1' : '0');
    }

    printf("Charcode: %d. Binary: %s\n", x, b);

    return b;
}

int reverse_endianess(void *data, size_t length) {
    if (length % 2 != 0) return -1;

    uint8_t swap, *p_data = data;
    for (int i = 0; i < (length / 2); ++i) {
        swap = *(p_data + length - 1 - i);
        *(p_data + length - 1 - i) = *(p_data + i);
        *(p_data + i) = swap;
    }

    return 0;
}