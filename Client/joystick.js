var cv = document.getElementById("canvas_1");
var ctx = cv.getContext("2d");

var timer = null; //Draw
var cb = null; //callback for joystick position change

var drag = false;
var update_canvas = true;
var default_circle_position = {x: window.innerWidth / 2, y: window.innerHeight / 2};
var cursorPos = default_circle_position;

//Constants
var radius_fallback_circle = 100;
var radius_joystick_circle = 50;

/**
 * Cleans the canvas completely
 * */
function resetCanvas() {
    ctx.beginPath();
    ctx.clearRect(0, 0, cv.width, cv.height);
}

/**
 * Checks if a point is in a circle
 * */
function pointInCircle(x, y, cx, cy, radius) {
    var distance_squared = (x - cx) * (x - cx) + (y - cy) * (y - cy);
    return distance_squared <= radius * radius;
}

function toRadians(angle) {
    return angle * (Math.PI / 180);
}

function toDegrees(angle) {
    return angle * (180 / Math.PI);
}

/**
 * Wrapper function so touch works (as well as mouse)
 * */
function getCoordinatesFromTouchEvent(event){
    var coordinates = {};
    coordinates.clientX = event.targetTouches[0].pageX;
    coordinates.clientY = event.targetTouches[0].pageY;
    return coordinates;
}

/**
 * This function calculates a point (x, y) on the edge of the big circle based on the
 * angle of the mouse in relation to the origin on the big circle.
 * */
function calculate_circle_edge_coordinates(cx, cy, ex, ey) {
    var dy = ey - cy;
    var dx = ex - cx;

    var angle = Math.atan2(dy, dx);
    var delta_x = Math.cos(angle) * radius_fallback_circle;
    var delta_y = Math.sin(angle) * radius_fallback_circle;
    return {x: default_circle_position.x + delta_x, y: default_circle_position.y + delta_y};
}

function getMousePos(event) {
    return {x: event.clientX, y: event.clientY};
}

/**
 * Wrapper function so touch works (as well as mouse)
 * */
function touchMove(event){
    mouseMove(getCoordinatesFromTouchEvent(event));
}

/**
 * When the mouse is moved the smaller circle follows it around, but only till
 * the edge of the bigger circle.
 * */
function mouseMove(event) {
    if (drag && !update_canvas) {
        cursorPos = getMousePos(event);

        //If the cursor is not in the fallback circle, draw joystick at the edge of the fallback circle
        if (!pointInCircle(cursorPos.x, cursorPos.y, default_circle_position.x, default_circle_position.y, radius_fallback_circle)) {
            cursorPos = calculate_circle_edge_coordinates(default_circle_position.x, default_circle_position.y, cursorPos.x, cursorPos.y);
        }
        update_canvas = true;
    }
}

/**
 * Wrapper function so touch works (as well as mouse)
 * */
function touchClick(event) {
    mouseClick(getCoordinatesFromTouchEvent(event));
}

/**
 * When a mouse is clicked inside the joystick, the smaller circle
 * will become draggable*/
function mouseClick(event) {
    cursorPos = getMousePos(event);
    if (pointInCircle(cursorPos.x, cursorPos.y, window.innerWidth / 2, window.innerHeight / 2, 50)) {
        drag = true;
    }
}

/**
 * Wrapper function so touch works (as well as mouse)
 * */
function touchReleased(event){
    mouseReleased();
}

/**
 * When the mouse (or touch) is release the joystick will snap back to its original position.
 * */
function mouseReleased() {
    cursorPos = default_circle_position;
    update_canvas = true;
    drag = false;
}

/**
 * This function updates the canvas, which means it will draw only if necessary.
 * */
function draw() {
    if (update_canvas) {
        resetCanvas();
        draw_fallback_circle();
        draw_joystick_circle(cursorPos.x, cursorPos.y);
        update_canvas = false;

        if (cb !== null) {
            handle_callback_request();
        }
    }
}

function draw_fallback_circle() {
    draw_circle(default_circle_position.x, default_circle_position.y, radius_fallback_circle, 'blue');
}

function draw_joystick_circle(x, y) {
    draw_circle(x, y, radius_joystick_circle, 'green');
}

function draw_circle(x, y, r, color) {
    ctx.beginPath();
    ctx.arc(x, y, r, 0, 2 * Math.PI, false);
    ctx.fillStyle = color;
    ctx.fill();
    ctx.lineWidth = 5;
    ctx.strokeStyle = '#003300';
    ctx.stroke();
    ctx.closePath();
}

/**
 * The joystick consists of two circles, a big and a small one. The position of the small
 * circle relative to the center of the bigger circle is used as the range. This value is
 * then transformed into an easier understandable scaling, namely -100 to 100 for both axis.
 * */
function handle_callback_request() {
    var float_x = 0 - (default_circle_position.x - cursorPos.x) / radius_fallback_circle;
    var float_y = (default_circle_position.y - cursorPos.y) / radius_fallback_circle;
    float_x *= -1; //Inverted steering
    var result = {x: Math.round(float_x * 100), y: Math.round(float_y * 100)};
    cb(result);
}

/**
 * When the joystick coordinates change they are given to a callback method with the parameters:
 * x: range -100 to 100
 * y: range -100 to 100
 * */
function joystick_register(callback) {
    cb = callback;
}

/**
 * This function initialises the canvas and the joystick, meaning it will be visible on the screen.
 * */
function joystick_init() {
    //Aesthetics
    ctx.shadowColor = "grey";
    ctx.shadowOffsetX = 1;
    ctx.shadowOffsetY = 1;
    ctx.shadowBlur = 6;

    //Fill the entire screen
    ctx.canvas.width = window.innerWidth;
    ctx.canvas.height = window.innerHeight;

    timer = setInterval(draw, 50); //Updates canvas every 50 ms

    cv.addEventListener('mouseup', mouseReleased);
    cv.addEventListener('mousedown', mouseClick);
    cv.addEventListener('mousemove', mouseMove);

    //So it works on mobile
    cv.addEventListener('touchstart', touchClick);
    cv.addEventListener('touchmove', touchMove);
    cv.addEventListener('touchend', touchReleased);
}
