/* === TOOLS === */

/**
 * This function creates a packet using the specified protocol and returns that packet.
 * */
function create_packet(id, content) {
    var packet = new Uint8Array(content.length + 1);
    packet[0] = id;
    for (var i = 0; i < content.length; i++) {
        packet[i + 1] = content[i];
    }
    return packet;
}

/**
 * This function transforms an incoming bytestream to a usable packet and passes
 * that to the controller
 * */
function read_packet(packet) {
    var buffer = new Uint8Array(packet);
    var packet_id = buffer[0];
    var content = buffer.slice(1);
    console.log("Found packet id: " + packet_id + ", with content: " + content);
    controller(packet_id, content)
}

/* === MAIN === */

var hostname = "192.168.42.1";
var port = "53232";
var connected = false;

console.log("Started");

var connect_string = "ws://" + hostname + ":" + port + "/";
var server_socket = new WebSocket(connect_string);
server_socket.binaryType = "arraybuffer";
var send_interval = false;

server_socket.onopen = function () {
    console.log("Handshake successful");
    connected = true;

    joystick_init();
    joystick_register(handle_new_speed);
};

server_socket.onmessage = function (msg) {
    console.log("Got message: " + msg.data);
    controller(read_packet(msg.data));
};

server_socket.onclose = function () {
    console.log("Server closed connection");
};

/* === LOGIC === */

/**
 * The controller handles the logic of all incoming packets
 * */
function controller(packet_id, payload) {
    if (packet_id === 1) {
        //Debugging
        console.log(String.fromCharCode(payload[0]));
        console.log(String.fromCharCode(payload[1]));
    } else if (packet_id === 2) {
        var temp = (payload[1] << 8) | payload[0];
        console.log("LDR");
        console.log(temp);
    }
}

/**
 * This is the callback method from joystick_register_callback. This means
 * that every time that the joystick has a new result this method will be called.
 * This method then wraps the coordinates according to the protocol and sends it to
 * the server at an interval caused by the queue timer. Too many messages to the server
 * will overflow the connection.
 * */
function handle_new_speed(result) {
    console.log(result);
    var packet = new Int8Array(3);
    packet[0] = 1;
    packet[1] = result.x;
    packet[2] = result.y;

    if (send_interval || (result.x === 0 && result.y === 0)) {
        send_interval = false;
        if (connected) server_socket.send(packet);
    }

}

/**
 * Throttle the outgoing messages to keep a more stable connection.
 * */
function queue() {
    send_interval = true;
}

timer = setInterval(queue, 300);