#include <ESP8266WiFi.h>
#include <SoftwareSerial.h>

#include <stdint.h>

const char* ssid = "RobbinPi";
const char* password = "henksupermasterfrog";
const char* host = "192.168.42.1";
const int port = 58081;

const bool DEBUG = false;
uint8_t i, payloadSize;

WiFiClient client;
SoftwareSerial ardSerial(D1, D2);

void setup() {
  Serial.begin(9600);
  ardSerial.begin(9600);
  delay(10); //Settle

  pinMode(BUILTIN_LED, OUTPUT);

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }
}

void loop() {
  if (!client.connect(host, port)) {
    delay(1000);
    return; //to loop
  }

  while (1) {
    checkServerBuffer();
    checkArduinoBuffer();
  }
}

uint8_t getByte() {
  while (1) {
    if (client.available()) return client.read();
    else delay(1);
  }
}

void checkServerBuffer() {
  if (!client.available()) return;
  payloadSize = getByte();

  if (payloadSize != 0) {
    if (DEBUG) {
      Serial.print("New packet, length: ");
      Serial.println(String(payloadSize));
    }

    uint8_t buffer[payloadSize];
    for (i = 0; i < payloadSize; i++) {
      buffer[i] = getByte();
      if (DEBUG) {
        Serial.println(String((int8_t) buffer[i]));
      }
    }

    ardSerial.write(payloadSize);
    ardSerial.write(buffer, payloadSize);

  } else {
    if (DEBUG) Serial.println("Received a zero length packet");
  }
}

void checkArduinoBuffer() {
  if (!ardSerial.available()) return;
  while (ardSerial.available()) {
    byte data = ardSerial.read();
    client.write(data);
    if (DEBUG) Serial.write(data);
  }
}
