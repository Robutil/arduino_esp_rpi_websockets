#include <SoftwareSerial.h>
#include <SharpIR.h>

#include <stdint.h>

SoftwareSerial espSerial(2, 3);

//4096 steps in a full rotation
#define MAX_STEER_ROTATIONS 3000

#define PACKET_SPEED 1
#define LED 13
#define IN1  8 //Stepper
#define IN2  9 //Stepper
#define IN3  10 //Stepper
#define IN4  11 //Stepper
#define MOTOR 4 //Main
#define COUNTER_SERIAL 250

const bool DEBUG = false;

//Vars for stepper engine (steering)
int currentStep = 0;
int desiredStep = 0;
int cycleCounter = 0;

//Vars for main engine
int motorPower = 0;
int motorOnTime = 1, motorOffTime = 1, motorOn = 0, motorOff = 0;
bool motorState = true;

//Vars for lrd
int LDRPin = A0;
int LDRValue = 0;
int KoplampLed2 = 12;
long previousTimeLDR = 0;
long intervalLDR = 10000;

//DelayCounters
int serialCounter = 0;

void setup() {
  espSerial.begin(9600);
  //Serial.begin(9600);
  
  pinMode(LED, OUTPUT);
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
  pinMode(MOTOR, OUTPUT);
  pinMode(KoplampLed2, OUTPUT);
  pinMode(LDRPin, INPUT);
}

void loop() {
  checkSerial();
  checkSteering();
  checkMainMotor();
  checkLdr();
  delay(1);
}

void checkSerial() {
  serialCounter++;
  if (serialCounter < COUNTER_SERIAL) return;
  else serialCounter = 0;

  if (espSerial.available() <=  1) return;
  byte bytes_to_read = espSerial.read();
  byte buffer[bytes_to_read];
  byte read_bytes = 0;

  while (read_bytes < bytes_to_read) {
    read_bytes += espSerial.readBytes(&buffer[read_bytes], bytes_to_read - read_bytes);
  }

  if (buffer[0] == PACKET_SPEED) {
    int8_t x = buffer[1];
    int8_t y = buffer[2];

    desiredStep = (x / 100.0f) * MAX_STEER_ROTATIONS;

    //No negative values for the engines yet
    if (y < 0) motorPower = 0;
    else motorPower = y;

    if (DEBUG) {
      espSerial.print("x:");
      espSerial.println(String(x));
      espSerial.print("d:");
      espSerial.println(String(desiredStep));
    }
  }
}

void checkSteering() {
  if (desiredStep < currentStep) {
    negativeCycle();
  } else if (desiredStep > currentStep) {
    positiveCycle();
  }
}

void positiveCycle() {
  currentStep++;
  cycleCounter++;
  performStep();
}

void negativeCycle() {
  currentStep--;
  cycleCounter--;
  performStep();
}


void performStep() {
  if (cycleCounter > 7) cycleCounter = 0;
  else if (cycleCounter < 0) cycleCounter = 7;

  switch (cycleCounter) {
    case 0:
      digitalWrite(IN1, LOW);
      digitalWrite(IN2, LOW);
      digitalWrite(IN3, LOW);
      digitalWrite(IN4, HIGH);
      break;
    case 1:
      digitalWrite(IN1, LOW);
      digitalWrite(IN2, LOW);
      digitalWrite(IN3, HIGH);
      digitalWrite(IN4, HIGH);
      break;
    case 2:
      digitalWrite(IN1, LOW);
      digitalWrite(IN2, LOW);
      digitalWrite(IN3, HIGH);
      digitalWrite(IN4, LOW);
      break;
    case 3:
      digitalWrite(IN1, LOW);
      digitalWrite(IN2, HIGH);
      digitalWrite(IN3, HIGH);
      digitalWrite(IN4, LOW);
      break;
    case 4:
      digitalWrite(IN1, LOW);
      digitalWrite(IN2, HIGH);
      digitalWrite(IN3, LOW);
      digitalWrite(IN4, LOW);
      break;
    case 5:
      digitalWrite(IN1, HIGH);
      digitalWrite(IN2, HIGH);
      digitalWrite(IN3, LOW);
      digitalWrite(IN4, LOW);
      break;
    case 6:
      digitalWrite(IN1, HIGH);
      digitalWrite(IN2, LOW);
      digitalWrite(IN3, LOW);
      digitalWrite(IN4, LOW);
      break;
    case 7:
      digitalWrite(IN1, HIGH);
      digitalWrite(IN2, LOW);
      digitalWrite(IN3, LOW);
      digitalWrite(IN4, HIGH);
      break;
    default:
      digitalWrite(IN1, LOW);
      digitalWrite(IN2, LOW);
      digitalWrite(IN3, LOW);
      digitalWrite(IN4, LOW);
      break;
  }
}

void checkMainMotor() {
  float power;
  if (motorPower > 0) {
    power = (10 * (motorPower / 100.0f)) * 10;
  } else {
    power = 0;
  }
  motorOn = power;
  motorOff = 10 - power;

  if (!motorState) {
    if (motorOffTime <= 0) {
      motorState = true;
      motorOnTime = motorOn;
    } else {
      motorOffTime--;
    }
    digitalWrite(MOTOR, LOW);
  } else {
    if (motorOnTime <= 0) {
      motorState = false;
      motorOffTime = motorOff;
    } else {
      motorOnTime--;
    }
    digitalWrite(MOTOR, HIGH);
  }
}

void checkLdr() {
  unsigned long currentTimeLDR = millis();
  if (currentTimeLDR - previousTimeLDR > intervalLDR) {
    previousTimeLDR = currentTimeLDR;

    LDRValue = analogRead(LDRPin);

    //Send info to server
    espSerial.write(3);
    espSerial.write(2);
    espSerial.write(LDRValue & 0x00FF);
    espSerial.write((LDRValue & 0xFF00) >> 8);

    if(DEBUG) Serial.println(String(LDRValue));

    if (LDRValue < 40) { // beetje met waarde spelen, 200 was best ok > threshold 40 for lights when using a resistor
      digitalWrite(LED, HIGH);
    }    else {
      digitalWrite(LED, LOW);
    }
  }
}
